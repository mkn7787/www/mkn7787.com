---
title: Home
menu:
  main:
    weight: 100
---

# Services

We specialize in scientific software and the ecosystem around it.
Some example services:

* conda-forge packaging
* CI/CD pipeline setup
* Containerization

We also provide python, command line, and git training.
